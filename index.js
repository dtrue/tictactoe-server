var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(8000);

app.get('/', function (req, res) {
	res.send("Hello");
});

var playersActive = {};
var usernames = {};

io.on('connection', function (socket) {
	var id = Math.floor(Math.random() * 1000000000);
	playersActive[id] = socket;
	socket.emit('id', { id: id });
	var myUserName;

	socket.on("registerUsername", function (username, fn) {
		//delete old username
		delete usernames[myUserName];
		if (usernames[username]) {
			fn('username-taken');
			return;
		}
		usernames[username] = id;
		myUserName = username;
		socket.broadcast.emit("newPlayer", JSON.stringify({
			id: id,
			username: username
		}))
		fn('ok')
	});

	socket.on("getUsersInLobby", function (fn) {
		fn(JSON.stringify(usernames));
	});

	var currentOpponent;


	socket.on('userSelected', function (usernameToPlay, fn) {
		var personToPlayId = usernames[usernameToPlay];
		var personToPlaySocket = playersActive[personToPlayId];
		personToPlaySocket.emit("gameStarted", JSON.stringify({ id: id, username: myUserName }));

		currentOpponent = {
			id: personToPlayId,
			username: usernameToPlay,
			socket: personToPlaySocket
		}
		fn('done');
	});

	socket.on('gameStartAcknowledged', function (usernameToPlay, fn) {
		console.log("user is ", usernameToPlay)
		var personToPlayId = usernames[usernameToPlay];
		var personToPlaySocket = playersActive[personToPlayId];
		currentOpponent = {
			id: personToPlayId,
			username: usernameToPlay,
			socket: personToPlaySocket
		}
	})

	socket.on('moved', function (move) {
		currentOpponent.socket.emit("opponentMoved", move);

	})

	socket.on('disconnect', function () {
		delete playersActive[id]
		delete usernames[myUserName];
	})
});